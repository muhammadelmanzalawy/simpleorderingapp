package com.example.mohamed.simpleorderingapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class OrderingActivity extends AppCompatActivity {

    //    Public Variables
    String msg = "";
    int price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ordering);

        final Button orderButton = (Button) findViewById(R.id.order_button);
//        Setting the LongPress listener
        orderButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(OrderingActivity.this,
                        "Your Order's Total Price: " + price, Toast.LENGTH_LONG).show();
                return true;
            }
        });
    }


    public void orderMeal (View view){
        EditText orderEditText = (EditText) findViewById(R.id.order_edit_text);
        int mealNumber = 0;
        if (orderEditText.getText().length() != 0) {
            mealNumber = Integer.parseInt(orderEditText.getText().toString());
        }
        switch (mealNumber){
            case 1:
                msg = msg + "\nStarters";
                price = price + 15;
                updateOrder();
                break;
            case 2:
                msg = msg + "\nSide Dish";
                price = price + 25;
                updateOrder();
                break;
            case 3:
                msg = msg + "\nMain Dish";
                price = price + 45;
                updateOrder();
                break;
            case 4:
                msg = msg + "\nDrink";
                price = price + 8;
                updateOrder();
                break;
            case 5:
                msg = msg + "\nDesert";
                price = price + 20;
                updateOrder();
                break;
        }
    }

    //    This method is for updating the order MSG
    private void updateOrder() {
        TextView orderTextView = (TextView) findViewById(R.id.order_text_view);
        TextView priceTextView = (TextView) findViewById(R.id.price_text_view);

        orderTextView.setText("Your Order:" + msg);
        priceTextView.setText("Total: " + price);
    }
}